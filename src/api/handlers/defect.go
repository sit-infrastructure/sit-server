package handlers

import (
	"fmt"
	"time"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"encoding/base64"
	"strconv"
	"api/db"
	"api/s3"
)


func SearchDefectHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>

	          select ?defect ?description ?datetime ?image ?inspection ?inspectionName ?inspector ?inspectorFirstName ?inspectorLastName ?priority ?latitude ?longitude ?draft ?se ?sename ?dt ?dtname ?ld ?ldname where {`

	if val, ok := requestMap["structuralElement"]; ok {
		if (val != nil) {
			query = query + `
										?defect sit:defectStructuralElement ?sefilt .
										{
											?sefilt rdf:type <`+ val.(string) +`>
										}
										union
										{
											?element rdfs:subClassOf+ ?parent .
												?parent rdf:type <`+ val.(string) +`>
										}`
		}
	}

	if val, ok := requestMap["inspection"]; ok {
		if (val != nil) {
			query = query + `?defect sit:defectInspection <`+ val.(string) +`> .`
		}
	}

	if val, ok := requestMap["inspector"]; ok {
		if (val != nil) {
			query = query + `?defect sit:defectInspector <`+ val.(string) +`> .`
		}
	}

	if val, ok := requestMap["description"]; ok {
		if (val != nil) {
			query = query + `	?defect sit:defectDescription ?description .
												filter (regex(?description, "`+ val.(string) +`")) .`
		}
	}

	if val, ok := requestMap["datetime"]; ok {
		if (val != nil) {
			query = query + `?defect sit:defectDatetime ?date .
									  	 filter (?date < "`+ val.(string) +`"^^xsd:dateTime) .`
		}
	}

	if val, ok := requestMap["isDraft"]; ok {
		if (val != nil) {
			query = query + `?defect sit:defectIsDraft ?draft .
											 filter (?draft = "`+ val.(string) +`"^^xsd:boolean) .`
		}
	}

	if val, ok := requestMap["defectType"]; ok {
		if (val != nil) {
			query = query + 		`?defect sit:defectDefectType ?dtfilt .
													{
														?dtfilt rdf:type <`+ val.(string) +`>
													}
													union
													{
														?dtfilt rdfs:subClassOf+ ?parent .
															?parent rdf:type <`+ val.(string) +`>
													}`
		}
	}

	if val, ok := requestMap["locationDescription"]; ok {
		if (val != nil) {
			query = query + `?defect sit:defectLocationDescription ?ldfilt .
													{
														?ldfilt rdf:type <`+ val.(string) +`>
													}
													union
													{
														?ldfilt rdfs:subClassOf+ ?parent .
															?parent rdf:type <`+ val.(string) +`>
													}`
		}
	}

	if val, ok := requestMap["priority"]; ok {
		if (val != nil) {
			query = query + `?defect sit:defectPriority ?priority .
											 filter (?priority >=`+ val.(string) +`) .`
		}
	}

	query = query + ` ?defect sit:defectInspection ?inspection .
									  ?inspection sit:inspectionName ?inspectionName .
									  ?defect sit:defectInspector ?inspector .
									  ?inspector sit:inspectorFirstName ?inspectorFirstName .
									  ?inspector sit:inspectorLastName ?inspectorLastName .
										?defect sit:defectDescription ?description .
									  ?defect sit:defectDatetime ?datetime .
									  ?defect sit:defectPriority ?priority .
									  ?defect sit:defectIsDraft ?draft .
										?defect sit:defectImage ?image .
										optional { ?defect sit:defectLatitude ?latitude }
										optional { ?defect sit:defectLongitude ?longitude }
										optional
										{
											?defect sit:defectStructuralElement ?se .
											?se sit:hierarchyElementName ?sename
										}
										optional
									  {
									    ?defect sit:defectDefectType ?dt .
									    ?dt sit:hierarchyElementName ?dtname
									  }
										optional
									  {
									    ?defect sit:defectLocationDescription ?ld .
									    ?ld sit:hierarchyElementName ?ldname
									  }

									}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func InsertDefectHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	timestamp := int(time.Now().Unix())

	data, err := base64.StdEncoding.DecodeString(requestMap["image"].(string))

	upload := s3.UploadToS3(data, strconv.Itoa(timestamp))

	query := `prefix sit: <http://api.stardog.com/SIT#>
						insert {
						_:node rdf:type sit:Defect . `

	if val, ok := requestMap["structuralElement"]; ok {
		if (val != nil) {
			query = query + `_:node sit:defectStructuralElement <`+ requestMap["structuralElement"].(string) + `> .`
		}
	}

	if val, ok := requestMap["defectType"]; ok {
		if (val != nil) {
			query = query + `_:node sit:defectDefectType <` + requestMap["defectType"].(string) + `> . `
		}
	}

	if val, ok := requestMap["locationDescription"]; ok {
		if (val != nil) {
			query = query + `_:node sit:defectLocationDescription <`+ requestMap["locationDescription"].(string) +`> . `
		}
	}

	query = query +  `_:node sit:defectInspector <`+ requestMap["inspector"].(string) +`> .
										_:node sit:defectInspection  <`+ requestMap["inspection"].(string) +`> .
										_:node sit:defectDescription "`+ requestMap["description"].(string) +`" .
										_:node sit:defectDatetime "`+ requestMap["date"].(string) + `"^^xsd:dateTime .
										_:node sit:defectPriority `+ requestMap["priority"].(string) +` .`

	if val, ok := requestMap["latitude"]; ok {
		if (val != nil) {
			query = query + `_:node sit:defectLatitude `+ requestMap["latitude"].(string) +` .`
		}
	}

	if val, ok := requestMap["longitude"]; ok {
		if (val != nil) {
			query = query + `_:node sit:defectLongitude `+ requestMap["longitude"].(string) +` .`
		}
	}

	if val, ok := requestMap["altitude"]; ok {
		if (val != nil) {
			query = query + `_:node sit:defectAltitude `+ requestMap["altitude"].(string) + ` .`
		}
	}

	query = query + `_:node sit:defectIsDraft "`+ requestMap["isDraft"].(string) +`"^^xsd:boolean .
									 _:node sit:defectImage "`+ upload + `" .
									 }
									 where {
									 }`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)

}

func DeleteDefectHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						delete where { <`+ requestMap["defect"].(string) +`> ?a ?b }`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)

}

func UpdateDefectHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	timestamp := int(time.Now().Unix())

	data, err := base64.StdEncoding.DecodeString(requestMap["image"].(string))

	upload := s3.UploadToS3(data, strconv.Itoa(timestamp))

	query := `prefix sit: <http://api.stardog.com/SIT#>
						insert data {
						<`+ requestMap["defect"].(string) +`> rdf:type sit:Defect . `

	if val, ok := requestMap["structuralElement"]; ok {
		if (val != nil) {
			query = query + `<`+ requestMap["defect"].(string) +`> sit:defectStructuralElement <`+ requestMap["structuralElement"].(string) + `> .`
		}
	}

	if val, ok := requestMap["defectType"]; ok {
		if (val != nil) {
			query = query + `<`+ requestMap["defect"].(string) +`> sit:defectDefectType <` + requestMap["defectType"].(string) + `> . `
		}
	}

	if val, ok := requestMap["locationDescription"]; ok {
		if (val != nil) {
			query = query + `<` + requestMap["defect"].(string) +`> sit:defectLocationDescription <`+ requestMap["locationDescription"].(string) +`> . `
		}
	}

	query = query + `<` + requestMap["defect"].(string) +`> sit:defectInspector <`+ requestMap["inspector"].(string) +`> .
									<`+ requestMap["defect"].(string) +`> sit:defectInspection  <`+ requestMap["inspection"].(string) +`> .
									<`+ requestMap["defect"].(string) +`> sit:defectDescription "`+ requestMap["description"].(string) +`" .
									<`+ requestMap["defect"].(string) +`> sit:defectDatetime "`+ requestMap["date"].(string) + `"^^xsd:dateTime .
									<`+ requestMap["defect"].(string) +`> sit:defectPriority `+ requestMap["priority"].(string) +` .
									<`+ requestMap["defect"].(string) +`> sit:defectLatitude `+ requestMap["latitude"].(string) +` .
									<`+ requestMap["defect"].(string) +`> sit:defectLongitude `+ requestMap["longitude"].(string) +` .
									<`+ requestMap["defect"].(string) +`> sit:defectIsDraft "`+ requestMap["isDraft"].(string) +`"^^xsd:boolean .
									<`+ requestMap["defect"].(string) +`> sit:defectImage "`+ upload + `" .
									}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)

}
