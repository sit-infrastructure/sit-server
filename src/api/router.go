package main

import (
	"github.com/gorilla/mux"
	"api/handlers"
)

func NewRouter() *mux.Router {
	r := mux.NewRouter()
	// Inspection Endpoints
  r.HandleFunc("/inspection/all", handlers.ListInspectionHandler)
	r.HandleFunc("/inspection/delete", handlers.DeleteInspectionHandler)
	r.HandleFunc("/inspection/structure", handlers.GetAllInspectionsByStructureHandler)
	r.HandleFunc("/inspection/insert", handlers.InsertInspectionHandler)
	r.HandleFunc("/inspection/associate", handlers.AssociateInspectionToStructureHandler)
	r.HandleFunc("/inspection/inspectors", handlers.GetAssignedInspectorsHandler)
	r.HandleFunc("/inspection/delete", handlers.DeleteInspectionHandler)

	// Structure Endpoints
	r.HandleFunc("/structure/delete", handlers.DeleteStructureHandler)
	r.HandleFunc("/structure/all", handlers.GetAllStructureHandler)
	r.HandleFunc("/structure/insert", handlers.InsertStructureHandler)
	r.HandleFunc("/structure/hierarchy/insert", handlers.AddHeirarchyToStructureHandler)
	r.HandleFunc("/structure/hierarchies", handlers.GetHierarchyForStructureHandler)
	r.HandleFunc("/structure/inspections", handlers.GetAllInspectionsByStructureHandler)

	// Search Endpoints
	r.HandleFunc("/defect/search", handlers.SearchDefectHandler)
	r.HandleFunc("/defect/insert", handlers.InsertDefectHandler)
	r.HandleFunc("/defect/delete", handlers.DeleteDefectHandler)
	r.HandleFunc("/defect/update", handlers.UpdateDefectHandler)

	// Inspector Endpoints
	r.HandleFunc("/inspector/inspections", handlers.GetAssignedInspectionsHandler)
	r.HandleFunc("/inspector/insert", handlers.InsertInspectorHandler)
	r.HandleFunc("/inspector/associate", handlers.AssociateInspectorToInspectionHandler)
	r.HandleFunc("/inspector/delete", handlers.DeleteInspectorHandler)
	r.HandleFunc("/inspector/glass", handlers.GetInspectorGlassID)
	r.HandleFunc("/inspector/all", handlers.GetAllInspectorsHandler)

	// Miscellaneous Hierarchy endpoints
	r.HandleFunc("/hierarchy/element/insert", handlers.AddElementToHierarchyHandler)
	r.HandleFunc("/hierarchy/elements", handlers.GetAllElementsFromHierarchyHandler)

	// User Management Endpoints
	r.HandleFunc("/login", handlers.LoginHandler)
	r.HandleFunc("/logout", handlers.LogoutHandler)
	r.HandleFunc("/account/create", handlers.NewAccountHandler)
	r.HandleFunc("/query/custom", handlers.CustomQueryHandler)

  return r
}
