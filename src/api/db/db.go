package db


import (
	"net/http"
	"net/url"
	"strings"
	"fmt"
  "io/ioutil"
	"encoding/json"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
)


/*
 * Query the Stardog database
 */
func QueryStardogDatabase(query string, useJSON string) string {
	client := &http.Client{}

	form := url.Values{}
	form.Add("query", query)

	req, _ := http.NewRequest("POST", "http://54.175.40.59:5820/dev/query", strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	if (useJSON == "true") {
			req.Header.Add("accept",  "application/sparql-results+json")
	}
	req.Header.Add("Authorization", "Basic YWRtaW46YWRtaW4=")

	resp, err := client.Do(req)

	if err != nil {
		obj := map[string]string{"status": "failure", "error": "Stardog database connetion failed with error: " + err.Error()}
		encoded, _ := json.Marshal(obj)
		return string(encoded)
	}

	defer resp.Body.Close()
	resp_body, _ := ioutil.ReadAll(resp.Body)

	return string(resp_body)
}

/*
 * Query the MySQL database
 */
func QueryMySQLDatabase(query string) map[string]string {
  db, err := sql.Open("mysql", "situser:guitarplayer823@/sit")
	if err != nil {
			fmt.Println("Error Querying the SQL db. Error:" + err.Error())
	}

  rows, err := db.Query(query)
	if (err != nil) {
		fmt.Println("Error inserting into DB! Error: " + err.Error())
	}
	resp := make(map[string]string)
	if (rows != nil) {
		columns, err := rows.Columns()
		if err != nil {
				panic(err.Error())
		}

		values := make([]sql.RawBytes, len(columns))
		scanArgs := make([]interface{}, len(values))
		for i := range values {
				scanArgs[i] = &values[i]
		}

		for rows.Next() {
				err = rows.Scan(scanArgs...)
				if err != nil {
						panic(err.Error())
				}

				var value string
				for i, col := range values {
						if col == nil {
								value = "NULL"
						} else {
								value = string(col)
						}

						resp[columns[i]] = value
				}
		}
	}
	return resp
}
