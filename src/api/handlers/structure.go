package handlers

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"api/db"
)

func GetAllStructureHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						select ?structure ?name ?seHier ?dtHier ?ldHier where {
						  ?structure rdf:type sit:Structure .
						  ?structure sit:structureName ?name .
						  ?structure sit:structureSEHierarchy ?seHier .
						  ?structure sit:structureDTHierarchy ?dtHier .
						  ?structure sit:structureLDHierarchy ?ldHier .
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func GetHierarchyForStructureHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>

						select ?sehierarchy ?ldhierarchy ?dthierarchy where {
						  <`+ requestMap["structure"].(string) +`> sit:structureSEHierarchy ?sehierarchy .
						  <`+ requestMap["structure"].(string) +`> sit:structureDTHierarchy ?dthierarchy .
						  <`+ requestMap["structure"].(string) +`> sit:structureLDHierarchy ?ldhierarchy .
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func InsertStructureHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>

						insert {
						  _:structure rdf:type sit:Structure .
						  _:structure sit:structureName "`+ requestMap["structure"].(string) +`" .
						  _:structure sit:structureSEHierarchy _:se .
						  _:structure sit:structureDTHierarchy _:dt .
						  _:structure sit:structureLDHierarchy _:ld .
						  _:se rdf:type sit:Class.
						  _:se rdfs:subClassOf sit:Hierarchy.
						  _:se rdf:type sit:Hierarchy.
						  _:se sit:hierarchyName "Structural Element".
						  _:dt rdf:type sit:Class.
						  _:dt rdfs:subClassOf sit:Hierarchy.
						  _:dt rdf:type sit:Hierarchy.
						  _:dt sit:hierarchyName "Defect Type".
						  _:ld rdf:type sit:Class.
						  _:ld rdfs:subClassOf sit:Hierarchy.
						  _:ld rdf:type sit:Hierarchy.
						  _:ld sit:hierarchyName "Location Description".
						}
						where {
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func AddHeirarchyToStructureHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						insert {
						  _:hierarchy rdf:type sit:Class.
						  _:hierarchy rdfs:subClassOf sit:Hierarchy.
						  _:hierarchy rdf:type sit:Hierarchy.
						  _:hierarchy sit:hierarchyName "`+ requestMap["entityName"].(string) +`".
						  `+ requestMap["structure"].(string) +` sit:structureHierarchy _:hierarchy .
						} where {
						}`
		results := db.QueryStardogDatabase(query, requestMap["json"].(string))

		fmt.Fprintf(w, results)
}

func DeleteStructureHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						delete {
					  <`+ requestMap["structure"].(string) +`> ?strucProp ?strucObj .
					  ?inspection ?inspProp ?inspObj .
					  ?defect ?defProp ?defObj .
					  ?seHier ?seHierProp ?seHierObj .
					  ?seSub ?seSubProp ?seSubObj .
					  ?dtHier ?dtHierProp ?dtHierObj .
					  ?dtSub ?dtSubProp ?dtSubObj .
					  ?ldHier ?ldHierProp ?ldHierObj .
					  ?ldSub ?ldSubProp ?ldSubObj .
					}
					where {
					  {
					    <`+ requestMap["structure"].(string) +`> ?strucProp ?strucObj .
					    <`+ requestMap["structure"].(string) +`> sit:structureInspection ?inspection .
					    ?inspection ?inspProp ?inspObj .
					    ?defect sit:defectInspection ?inspection .
					    ?defect ?defProp ?defObj .
					  }
					  union
					  {
					    <`+ requestMap["structure"].(string) +`> sit:structureSEHierarchy ?seHier .
					    ?seHier ?seHierProp ?seHierObj .
					    ?seSub rdfs:subClassOf+ ?seHier .
					    ?seSub ?seSubProp ?seSubObj .
					  }
					  union
					  {
					    <`+ requestMap["structure"].(string) +`> sit:structureDTHierarchy ?dtHier .
					    ?dtHier ?dtHierProp ?dtHierObj .
					    ?dtSub rdfs:subClassOf+ ?dtHier .
					    ?dtSub ?dtSubProp ?dtSubObj .
					  }
					  union
					  {
					    <`+ requestMap["structure"].(string) +`> sit:structureLDHierarchy ?ldHier .
					    ?ldHier ?ldHierProp ?ldHierObj .
					    ?ldSub rdfs:subClassOf+ ?ldHier .
					    ?ldSub ?ldSubProp ?ldSubObj .
					  }
					}`

		results := db.QueryStardogDatabase(query, requestMap["json"].(string))

		fmt.Fprintf(w, results)
}
