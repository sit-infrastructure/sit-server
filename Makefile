GOPATH := ${PWD}
GOROOT := /usr/local/bin/golang/

build-dev:
	GOPATH=$(GOPATH) go get github.com/gorilla/mux/...;
	GOPATH=$(GOPATH) go get github.com/mitchellh/goamz/...;
	GOPATH=$(GOPATH) go get github.com/go-sql-driver/mysql/...;
	GOPATH=$(GOPATH) go get golang.org/x/crypto/bcrypt/...;
	cd src/api && GOPATH=$(GOPATH) go build -o sit-api -v

build-master:
	GOPATH=$(GOPATH) go get github.com/gorilla/mux/...;
	GOPATH=$(GOPATH) go get github.com/mitchellh/goamz/...;
	GOPATH=$(GOPATH) go get github.com/go-sql-driver/mysql/...;
	GOPATH=$(GOPATH) go get golang.org/x/crypto/bcrypt/...;
	cd src/api && GOPATH=$(GOPATH) go install -o sit-api -v
