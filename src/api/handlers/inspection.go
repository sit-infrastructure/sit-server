package handlers

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"api/db"
)

// List Handler, to list all inspections
func ListInspectionHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						select ?inspection ?name where {
						  ?inspection rdf:type sit:Inspection .
						  ?inspection sit:inspectionName ?name .
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func GetAllInspectionsByStructureHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>

						select ?inspection ?name where {
						  <`+ requestMap["structure"].(string) +`> sit:structureInspection ?inspection .
						  ?inspection sit:inspectionName ?name .
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}


func InsertInspectionHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						insert {
						  _:inspection rdf:type sit:Inspection .
						  _:inspection sit:inspectionName "`+ requestMap["inspection"].(string) +`" .
						  <`+ requestMap["structure"].(string) +`> sit:structureInspection _:inspection .
						}
						where {
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}


func AssociateInspectionToStructureHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						insert data {
						  <`+ requestMap["structure"].(string) +`> sit:structureInspection <`+ requestMap["inspection"].(string) +`> .
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func GetAssignedInspectorsHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>

						select ?inspector ?first ?last where {
						  <`+ requestMap["inspection"].(string) +`> sit:inspectionInspector ?inspector .
						  ?inspector sit:inspectorFirstName ?first .
						  ?inspector sit:inspectorLastName ?last .
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func DeleteInspectionHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>

						delete {
						  ?structure sit:structureInspection <`+ requestMap["inspection"].(string) +`> .
						  <`+ requestMap["inspection"].(string) +`> ?a ?b .
						  ?defect ?c ?d .
						}
						where {
						  ?structure sit:structureInspection <`+ requestMap["inspection"].(string) +`> .
						  <INSPECTION_HERE> ?a ?b .
						  ?defect sit:defectInspection <`+ requestMap["inspection"].(string) +`> .
						  ?defect ?c ?d .
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}
