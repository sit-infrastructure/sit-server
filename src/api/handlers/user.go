package handlers

import (
	"fmt"
	"net/http"
	"time"
	"io/ioutil"
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"strconv"
	"strings"
	"regexp"
	"api/db"
)

// Log a User in to the system.
func LoginHandler(w http.ResponseWriter, r *http.Request) {
		var requestBody interface{}
		request, _ := ioutil.ReadAll(r.Body)
		obj := map[string]string{}

    err := json.Unmarshal([]byte(request), &requestBody)
		if (err != nil) {
			obj = map[string]string{"status": "failure", "error": err.Error()}
			encoded, _ := json.Marshal(obj)
			fmt.Fprintf(w, string(encoded))
			return
		}
		requestMap := requestBody.(map[string]interface{})

		password := db.QueryMySQLDatabase("SELECT password FROM sit.users WHERE username = '" + requestMap["username"].(string) + "'")
		err = bcrypt.CompareHashAndPassword([]byte(password["password"]), []byte(requestMap["password"].(string)))

		if(err == nil) {
			sessionKey := requestMap["username"].(string) + "-" + strconv.Itoa(int(time.Now().Unix()))
			db.QueryMySQLDatabase("UPDATE sit.users SET session_key = '" + sessionKey + "' WHERE username = '" + requestMap["username"].(string) + "'")
			userInfo := db.QueryMySQLDatabase("SELECT firstname, lastname, inspector_id FROM sit.users WHERE username = '" + requestMap["username"].(string) + "'")
			obj = map[string]string{"session_key":sessionKey, "firstname": userInfo["firstname"], "lastname": userInfo["lastname"], "inspector_id": userInfo["inspector_id"], "status": "success"}
		} else {
			obj = map[string]string{"status": "failure", "error": err.Error()}
		}

		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {

}

func NewAccountHandler(w http.ResponseWriter, r *http.Request) {
		var requestBody interface{}
		request, _ := ioutil.ReadAll(r.Body)
		obj := map[string]string{}

		err := json.Unmarshal([]byte(request), &requestBody)
		if (err != nil) {
			obj = map[string]string{"status": "failure", "error": err.Error()}
			encoded, _ := json.Marshal(obj)
			fmt.Fprintf(w, string(encoded))
			return
		}
		requestMap := requestBody.(map[string]interface{})

		if (!Authenticate(requestMap["user"].(string))) {
			obj = map[string]string{"status": "failure", "error": "403"}
			encoded, _ := json.Marshal(obj)
			fmt.Fprintf(w, string(encoded))
			return
		}

		identifier := strconv.Itoa(int(time.Now().Unix()))
		password, err := bcrypt.GenerateFromPassword([]byte(requestMap["password"].(string)) , 0)
		if (err != nil) {
			fmt.Println("Error generating Password.")
		}
		createUser := `INSERT INTO users (username, firstname, lastname, email, password, inspector_id, glass_id)
									 VALUES ('` + requestMap["username"].(string) + `',
														'`+ requestMap["firstname"].(string) + `',
														'`+ requestMap["lastname"].(string) +`',
														'`+ requestMap["email"].(string) +`',
														'`+ string(password) +`',
														'`+ identifier +`',
														'`+ requestMap["glass_id"].(string) +
										`');`

		db.QueryMySQLDatabase(createUser)

		userID := db.QueryMySQLDatabase("SELECT id FROM sit.users WHERE inspector_id = '" + identifier + "';")

		tempPredicate := `prefix sit: <http://api.stardog.com/SIT#>
											insert {
										  	_:node sit:fakeTempPredicate `+ identifier +`
											}
											where {
											}`
		db.QueryStardogDatabase(tempPredicate, "false")

		tempPredicate = `prefix sit: <http://api.stardog.com/SIT#>
										 select ?bnode where {
										 		?bnode sit:fakeTempPredicate `+ identifier +`
										 }`

		result := db.QueryStardogDatabase(tempPredicate, "true")

		stardogRegexp := regexp.MustCompile(`bnode_\w*`)
		result = string(stardogRegexp.Find([]byte(result)))

		updateUser := `UPDATE sit.users SET inspector_id = '`+ result  +`' WHERE id = ` + userID["id"] +`;`

		db.QueryMySQLDatabase(updateUser)

		deleteQuery := `prefix sit: <http://api.stardog.com/SIT#>
										delete where {
											<_:`+ result +`> sit:fakeTempPredicate ?tempid
										}`
		db.QueryStardogDatabase(deleteQuery, "true")

		newInspector := `prefix sit: <http://api.stardog.com/SIT#>
										  insert data {
											  <_:`+ result +`> rdf:type sit:Inspector .
											  <_:`+ result +`> sit:inspectorFirstName "`+ requestMap["firstname"].(string) +`" .
											  <_:`+ result +`> sit:inspectorLastName "`+ requestMap["lastname"].(string) +`" .
										 }`

		db.QueryStardogDatabase(newInspector, "true")

		obj = map[string]string{"username": requestMap["username"].(string), "inspector_id": result, "status": "success"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))

}


func AddElementToHierarchyHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						insert {
						  _:element rdf:type sit:Class.
						  _:element rdfs:subClassOf <` + requestMap["parent"].(string) + `> .
						  _:element rdf:type _:element .
						  _:element sit:hierarchyElementName " `+ requestMap["elementName"].(string) +`" .
						}
						where {
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func GetAllElementsFromHierarchyHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>

						select ?elementName ?parentName ?element ?parent where {
						  ?element rdfs:subClassOf+ <`+ requestMap["hierarchy"].(string) +`> .
						  ?element sit:hierarchyElementName ?elementName .
						  ?element rdfs:subClassOf ?parent .
						  optional {?parent sit:hierarchyElementName ?parentName}
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func CustomQueryHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	results := db.QueryStardogDatabase(requestMap["query"].(string), requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func Authenticate(key string) bool {
	s := strings.Split(key, "-")
	session := s[1]

	sessionInt, err := strconv.Atoi(session)
	if (err != nil) {
		return false;
	}

	now := int(time.Now().Unix())

	if (now - sessionInt > 2629743) { // The amount of seconds in a month.
		return false;
	} else {
		return true;
	}
}
