package s3

import (
  "fmt"
  "github.com/mitchellh/goamz/aws"
  "github.com/mitchellh/goamz/s3"
)


func UploadToS3(file []byte, fileName string) string {
  auth, err := aws.SharedAuth()
   if err != nil {
     fmt.Printf("Error uploading to S3. Error:" + err.Error())
   }
   client := s3.New(auth, aws.USEast)
   bucket := client.Bucket("sit-defects")
   bucket.Put(fileName, file, "jpeg", "public-read")

  if (err != nil) {
    return err.Error()
  } else {
    return fileName
  }
}
