# README #
### What is SIT? ###

The Structural Inspection Tool, or SIT, aims to modernize the bridge inspection process and eliminate the need for paper-based solutions. SIT is a team of four, all Drexel University Students developing SIT for the Senior Design 2015-2016 class. 

This specific repository deals with the server REST API, serving as the primary data pipeline to the complete system. It has been written in Go, with just a couple dependencies

### How do I get set up? ###

#### Dependencies ####

Currently, the server only requires two (2) dependencies to build and function.
* Gorilla Mux Router (http://www.gorillatoolkit.org/pkg/mux)
* AWS SDK (https://github.com/aws/aws-sdk-go)

Running `go get -u {package-url}/...` will install the package and all required sub-dependencies.

Once both of these packages are installed with dependencies, running `go install` should be sufficient to build the executable in the bin/ directory. Once that's created, just run it, and the router will launch on port 8080.

### Who do I talk to? ###

* Primary: Ryan Snider <rds74@drexel.edu>
* Jeff Ulman <jeffu92@gmail.com>