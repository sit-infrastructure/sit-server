package handlers

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"api/db"
)

func GetAssignedInspectionsHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>

						select ?inspection ?name where {
						  ?inspection sit:inspectionInspector <`+ requestMap["inspector"].(string) +`> .
						  ?inspection sit:inspectionName ?name .
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func InsertInspectorHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						insert {
						  _:inspector rdf:type sit:Inspector .
						  _:inspector sit:inspectorFirstName "`+ requestMap["firstname"].(string) +`" .
						  _:inspector sit:inspectorLastName "`+ requestMap["lastname"].(string) +`" .
						}
						where {
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}



func AssociateInspectorToInspectionHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						insert {
						  <`+ requestMap["inspection"].(string) +`> sit:inspectionInspector <`+ requestMap["inspector"].(string) +`> .
						}
						where {
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func DeleteInspectorHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						delete where {
						  <`+ requestMap["inspector"].(string) +`> ?a ?b .
						  ?inspection sit:inspectionInspector <`+ requestMap["inspector"].(string) +`> .
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func GetAllInspectorsHandler(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	query := `prefix sit: <http://api.stardog.com/SIT#>
						select ?inspector ?firstname ?lastname where {
						  ?inspector rdf:type sit:Inspector .
						  ?inspector sit:inspectorFirstName ?firstname .
						  ?inspector sit:inspectorLastName ?lastname
						}`

	results := db.QueryStardogDatabase(query, requestMap["json"].(string))

	fmt.Fprintf(w, results)
}

func GetInspectorGlassID(w http.ResponseWriter, r *http.Request) {
	var requestBody interface{}
	request, _ := ioutil.ReadAll(r.Body)
	obj := map[string]string{}

	err := json.Unmarshal([]byte(request), &requestBody)
	if (err != nil) {
		obj = map[string]string{"status": "failure", "error": err.Error()}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}
	requestMap := requestBody.(map[string]interface{})

	if (!Authenticate(requestMap["user"].(string))) {
		obj = map[string]string{"status": "failure", "error": "403"}
		encoded, _ := json.Marshal(obj)
		fmt.Fprintf(w, string(encoded))
		return
	}

	glassID := db.QueryMySQLDatabase("SELECT inspector_id FROM sit.users WHERE glass_id = '" + requestMap["glass_id"].(string) + "'")

	if(glassID != nil) {
		obj = map[string]string{"inspector_id":glassID["inspector_id"], "status": "success"}
	} else {
		obj = map[string]string{"status": "failure", "error": err.Error()}
	}

	encoded, _ := json.Marshal(obj)
	fmt.Fprintf(w, string(encoded))
}
